#!/bin/bash

# Deploy websites from buildbot

chmod -R ag+rX public/
DEPLOY_USER="reclaim"
rsync -a --delete public/ $DEPLOY_USER@firefly.gnunet.org:~/public/
